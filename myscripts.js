let puzzles = [];

let totalSeconds;
let index;
let sIndex;
var curentId;
var cancel = false;

var puzzleObj = {
    canvas: 0,
    ctx: 0,
    canvasw: 0,
    canvash: 0,
    savedcanvasw: 0,
    savedcanvash: 0,
    numberPiecesX: 0,
    numberPiecesY: 0,
    circleS: 0,
    puzzle: '',
    pieces: [],
    actualMoves: [],
    solvedpieces: [],
    clickedpiece: -1,
}


function verif() {

    // if (localStorage.getItem("savedPuzzles") !== null) {
    //     savedPuzzles = JSON.parse(localStorage.getItem('savedPuzzles'));
    //     sIndex = savedPuzzles.length;
    // }

    if (localStorage.getItem("puzzles") === null) {
        puzzles.push({
            id: 0,
            name: 'https://editiadedimineata.ro/wp-content/uploads/2018/05/lalele8.jpg',
            move: puzzleObj,
            status: 0
        });;
        puzzles.push({
            id: 1,
            name: 'https://cdn.toxel.ro/img/contents/Fotografii-flori-Barbara-Florczyk09.jpg',
            move: puzzleObj,
            status: 0
        });;
        puzzles.push({
            id: 2,
            name: 'https://1.bp.blogspot.com/-q2LXlilbtns/Vwpgf1zO14I/AAAAAAAAKJM/frdEcNI-Qb4RlGFIcFkYdVvwxBpOb_FLQ/s1600/copaci-primavara.jpg',
            move: puzzleObj,
            status: 0
        });;
        puzzles.push({
            id: 3,
            name: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR8QJHwuEXHtG2Dri8oyHKxD0Uw2WVtRjxJ4lh0A_L4r4i8WS97',
            move: puzzleObj,
            status: 0
        });;
        puzzles.push({
            id: 4,
            name: 'https://mesajededragosteblog.files.wordpress.com/2013/06/flori-imagini-poze-meisaje-frumoase-22.jpg',
            move: puzzleObj,
            status: 0 //nu s-a dat click pe el
        });;
        index = 5;


    } else {
        puzzles = JSON.parse(localStorage.getItem('puzzles'));
        index = puzzles.length;
    }
}

function savePuzzle(id) {
    puzzles[id].move =JSON.parse(JSON.stringify(puzzleObj));
    puzzles[id].status = 1; //este in categoria de neterminate
    localStorage.setItem('puzzles', JSON.stringify(puzzles));

    document.getElementById("message").innerHTML = "SAVED TO LOCALSTORAGE !";
    setTimeout(function() {
        document.getElementById("message").innerHTML = '';
    }, 3000);
}

function toSavedPuzzle(id) {
    verif();
    puzzles = JSON.parse(localStorage.getItem('puzzles'));
    puzzleObj = JSON.parse(JSON.stringify(puzzles[id].move));

    var div1 = document.getElementById("content");
    var div2 = document.getElementById("game");
    div1.style.display = "none";
    div2.style.display = "block";
    puzzleObj.canvas = document.getElementById('canvas');

    puzzleObj.canvas.width = puzzleObj.savedcanvasw;
    puzzleObj.canvas.height = puzzleObj.savedcanvash;
    puzzleObj.ctx = puzzleObj.canvas.getContext('2d');
    var result = puzzles.find(function(puzzle) {

        if (puzzle.id === id) {
            return true;
        } else {
            return false;
        }

    });
    var txt1=document.getElementById("message1").innerHTML= puzzleObj.solvedpieces.length + " from " + puzzleObj.numberPiecesX * puzzleObj.numberPiecesY;

    const wrapper2 = document.getElementById('buttons');
    const template = `

        <div class="gameS">
            <button class="buttonForm" id="back" type="submit" onclick="toMain()"> BACK </button>
            <button class="buttonForm" type="submit" onclick="savePuzzle(%ID%)">SAVE </button>
            <button class="buttonForm" type="submit" onclick="reset(%ID%)"> RESET </button>
        </div>
    `
    if (result) {
        wrapper2.innerHTML += template.replace('%ID%', result.id).replace('%ID%', result.id);
        console.log(wrapper2);
    } else {
        wrapper2.innerHTML += template.replace('%ID%', puzzles[size].id).replace('%ID%', puzzles[size].id);
    }

    events();
    drawPieces();
}
// ----- redirectare catre sectiunea cu jocul -----

function toPuzzle(id) {
    document.getElementById("startGame").disabled = true;
    var div1 = document.getElementById("content");
    var div2 = document.getElementById("game");
    div1.style.display = "none";
    div2.style.display = "block";
    canvasImg(id);
}

allPuzzle();

// ----- redirectare catre sectiunea home -----

function toMain() {
    var div1 = document.getElementById("content");
    var div2 = document.getElementById("game");
    div1.style.display = "block";
    div2.style.display = "none";
    localStorage.setItem('puzzles', JSON.stringify(puzzles));
    document.getElementById('mainSection').innerHTML = '';
    document.getElementById('unfinished').innerHTML = '';
    document.getElementById('buttons').innerHTML = '';
    allPuzzle();
}

function initCanvasSize() {
    var parentel = document.getElementById('canvas');
    var sizes;
    puzzleObj.canvas.width = puzzleObj.canvasw = window.innerWidth / 1.5;
    puzzleObj.canvas.height = puzzleObj.canvash = window.innerHeight / 1.7;
}


function randomS() {
    puzzleObj.circleS = Math.random() + 1.7;
    puzzleObj.numberPiecesX = Math.floor(Math.random() * 4) + 2;
    puzzleObj.numberPiecesY = Math.floor(Math.random() * 4) + 2;
}

function NewPiece(x, y, w, h, solvedx, solvedy, rowx, rowy) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.solvedx = solvedx;
    this.solvedy = solvedy;
    this.visible = 1;
    this.solved = 0;
    this.offsetx = -1;
    this.offsety = -1;
    this.rowx = rowx;
    this.rowy = rowy;
}


//creare piese

function createPieces() {
    puzzleObj.pieces = [];
    puzzleObj.solvedpieces = [];
    randomS();
    var w = puzzleObj.canvasw / puzzleObj.numberPiecesX;
    var h = puzzleObj.canvash / puzzleObj.numberPiecesY;

    var rangeminx = (puzzleObj.canvasw / 100) * 10;
    var rangemaxx = ((puzzleObj.canvasw - w) / 100) * 80;
    var rangeminy = (puzzleObj.canvash / 100) * 10;
    var rangemaxy = ((puzzleObj.canvash - h) / 100) * 80;
    for (var y = 0; y < puzzleObj.numberPiecesY; y++) {
        for (var x = 0; x < puzzleObj.numberPiecesX; x++) {

            var piecex = Math.random() * (rangemaxx - rangeminx) + rangeminx;
            var piecey = Math.random() * (rangemaxy - rangeminy) + rangeminy;
            var solvedx = w * x;
            var solvedy = h * y;
            var spritex = 0;
            var spritey = 0;

            var newpiece = new NewPiece(piecex, piecey, w, h, solvedx, solvedy, x, y);
            puzzleObj.pieces.push(newpiece);
        }
    }

}



function drawPieces() {

    puzzleObj.canvas.width = puzzleObj.canvas.width;

    var piececount = puzzleObj.solvedpieces.length;
    for (var p = 0; p < piececount; p++) {
        drawPiece(puzzleObj.solvedpieces[p]);
    }
    piececount = puzzleObj.pieces.length;
    for (var q = 0; q < piececount; q++) {
        drawPiece(puzzleObj.pieces[q]);
    }
}


function drawOnSide(obj, edge, arccounterClockwise) {
    var arcradius = Math.min(obj.h / puzzleObj.circleS, obj.w / puzzleObj.circleS);
    var arcx = 0;
    var arcy = 0;
    var arcstartAngle = 0;
    var arcendAngle = 0;
    switch (edge) {
        case 0:
            arcx = obj.x + (obj.w / 2);
            arcy = obj.y;
            arcstartAngle = 1 * Math.PI;
            arcendAngle = 0 * Math.PI;
            break;
        case 1:
            arcx = obj.x + obj.w;
            arcy = obj.y + (obj.h / 2);
            arcstartAngle = 1.5 * Math.PI;
            arcendAngle = 0.5 * Math.PI;
            break;
        case 2:
            arcx = obj.x + (obj.w / 2);
            arcy = obj.y + obj.h;
            arcstartAngle = 0 * Math.PI;
            arcendAngle = 1 * Math.PI;
            break;
        case 3:
            arcx = obj.x;
            arcy = obj.y + (obj.h / 2);
            arcstartAngle = 0.5 * Math.PI;
            arcendAngle = 1.5 * Math.PI;
            break;
    }
    puzzleObj.ctx.arc(arcx, arcy, arcradius, arcstartAngle, arcendAngle, arccounterClockwise);
}


function Even(r) {
    return r % 2 == 0;
}
function drawPiece(obj) {

    var pieceXEven = Even(obj.rowx);
    var pieceYEven = Even(obj.rowy);

    puzzleObj.ctx.save();
    if (obj.solved) {
        puzzleObj.ctx.lineWidth = 0;
        puzzleObj.ctx.strokeStyle = 'rgba(0,0,0,0)';
    } else {
        puzzleObj.ctx.lineWidth = 8;
        puzzleObj.ctx.strokeStyle = 'rgba(0,1,1,0.5)';
    }

    if (!obj.visible) {
        puzzleObj.ctx.globalAlpha = 0.2;
    }

    puzzleObj.ctx.beginPath();
    puzzleObj.ctx.moveTo(obj.x, obj.y); //up left

    //top edge
    if (obj.rowy > 0) {
        if (pieceYEven) {
            if (pieceXEven) {
                drawOnSide(obj, 0, 1); // out top 
            } else {
                drawOnSide(obj, 0, 0); //in top 
            }
        } else {
            if (pieceXEven) {
                drawOnSide(obj, 0, 0); //in top
            } else {
                drawOnSide(obj, 0, 1); //out top 
            }
        }
    }

    puzzleObj.ctx.lineTo(obj.x + obj.w, obj.y); //top right

    //right edge
    if (obj.rowx < puzzleObj.numberPiecesX - 1) {
        if (pieceYEven) {
            if (pieceXEven) {
                drawOnSide(obj, 1, 0); //in right 
            } else {
                drawOnSide(obj, 1, 1); //out right
            }
        } else {
            if (pieceXEven) {
                drawOnSide(obj, 1, 1); //out right
            } else {
                drawOnSide(obj, 1, 0); //in right
            }
        }
    }

    puzzleObj.ctx.lineTo(obj.x + obj.w, obj.y + obj.h); //bottom right

    //bottom edge
    if (obj.rowy < puzzleObj.numberPiecesY - 1) {
        if (pieceYEven) {
            if (pieceXEven) {
                drawOnSide(obj, 2, 1); //out bottom 
            } else {
                drawOnSide(obj, 2, 0); //in bottom
            }
        } else {
            if (pieceXEven) {
                drawOnSide(obj, 2, 0); //in bottom
            } else {
                drawOnSide(obj, 2, 1); //out bottom
            }
        }
    }

    puzzleObj.ctx.lineTo(obj.x, obj.y + obj.h); //bottom left

    //left edge
    if (obj.rowx > 0) {
        if (pieceYEven) {
            if (pieceXEven) {
                drawOnSide(obj, 3, 0); //in left 
            } else {
                drawOnSide(obj, 3, 1); //out left
            }
        } else {
            if (pieceXEven) {
                drawOnSide(obj, 3, 1); //out left
            } else {
                drawOnSide(obj, 3, 0); //in left
            }
        }
    }

    puzzleObj.ctx.lineTo(obj.x, obj.y); //top left
    puzzleObj.ctx.closePath();

    puzzleObj.ctx.clip();
    var puzzleImg = document.getElementById('puzzleImg');
    puzzleImg.src = puzzleObj.puzzle;
    puzzleObj.ctx.drawImage(puzzleImg, 0 - obj.solvedx + obj.x, 0 - obj.solvedy + obj.y, puzzleObj.canvasw, puzzleObj.canvash);
    puzzleObj.ctx.stroke();
    puzzleObj.ctx.restore();
}

// ----- afisare sectiunea cu puzzles -----

function allPuzzle() {
    const wrapper = document.getElementById('mainSection');
    const wrapper2 = document.getElementById('unfinished');
    const template = `

        <article class="puzzle">
            <figure class="flex-img">
                <img alt="image" src="%SRC%" />
            </figure>
            <button class="buttonSPuzzle" onclick="%NAME%(%ID%)">Puzzle me</button>
        </article>
        `

    verif();
    let size = puzzles.length;
    for (let i = 0; i < size; i++) {
        wrapper.innerHTML += template.replace('%NAME%', "toPuzzle").replace('%SRC%', puzzles[i].name).replace('%ID%', puzzles[i].id);
        if (puzzles[i].status == 1) {
            wrapper2.innerHTML += template.replace('%NAME%', "toSavedPuzzle").replace('%SRC%', puzzles[i].name).replace('%ID%', puzzles[i].id);
        }

    }

}


// ----- sectiunea cu jocul in canvas -----

function canvasImg(id) {

    puzzleObj.canvas = document.getElementById('canvas');
    //creare canvas

    if (!puzzleObj.canvas.getContext) {
        document.getElementById('canvas').innerHTML = 'Your browser does not support canvas';
    } else {
        puzzleObj.ctx = puzzleObj.canvas.getContext('2d');
        var result = puzzles.find(function(puzzle) {

            if (puzzle.id === id) {
                return true;
            } else {
                return false;
            }

        });

        var ig = new Image();
        let size = puzzles.length - 1;
        if (result) {
            ig.src = result.name;
            if (ig.width > window.innerWidth || ig.width < window.innerWidth / 2)
                ig.width = window.innerWidth / 1.5;
            if (ig.height > window.innerHeight || ig.height < window.innerHeight / 2)
                ig.height = window.innerHeight / 1.5;
        } else {
            ig.src = puzzles[size].name;
        }
        puzzleObj.puzzle = ig.src;

        ig.onload = function() {
            puzzleObj.puzzle = ig.src;
            puzzleObj.canvas.width = ig.width;
            puzzleObj.canvas.height = ig.height;
            initCanvasSize();
            puzzleObj.savedcanvasw = puzzleObj.canvasw;
            puzzleObj.savedcanvash = puzzleObj.canvash;
            createPieces();
            events();   
            var txt1=document.getElementById("message1").innerHTML= puzzleObj.solvedpieces.length + " from " + puzzleObj.numberPiecesX * puzzleObj.numberPiecesY;

            setInterval(drawPieces(), 10);
            const wrapper2 = document.getElementById('buttons');
            const template = `

                <div class="gameS">
                    <button class="buttonForm" id="back" type="submit" onclick="toMain()"> BACK </button>
                    <button class="buttonForm" type="submit" onclick="savePuzzle(%ID%)">SAVE </button>
                    <button class="buttonForm" type="submit" onclick="reset(%ID%)"> RESET </button>
                </div>
            `
            if (result) {
                wrapper2.innerHTML += template.replace('%ID%', result.id).replace('%ID%', result.id);
                console.log(wrapper2);
            } else {
                wrapper2.innerHTML += template.replace('%ID%', puzzles[size].id).replace('%ID%', puzzles[size].id);
            }


        };

    }
}

function reset(id) {
    document.getElementById('buttons').innerHTML = '';
    cancel = true;
    toPuzzle(id);
}

function events() {

    var ondown = ((document.ontouchstart !== null) ? 'mousedown' : 'touchstart');
    puzzleObj.canvas.addEventListener(ondown, function(e) {
        var clicked = clickDown(e);
        //console.log('click on');
        clickPiece(clicked[0], clicked[1]);
    }, false);


    var onup = ((document.ontouchstart !== null) ? 'mouseup' : 'touchend');
    puzzleObj.canvas.addEventListener(onup, function(e) {
        //console.log('click up');
        releasePiece();
    }, false);


    var onmove = ((document.ontouchstart !== null) ? 'mousemove' : 'touchmove');
    puzzleObj.canvas.addEventListener(onmove, function(e) {
        if (puzzleObj.clickedpiece !== -1) {
            movePiece(e);
            //console.log('on move');
        }
    }, false);


}


function hideAllPieces() {
    //console.log('hideAllPieces');
    for (var p = 0; p < puzzleObj.pieces.length; p++) {
        puzzleObj.pieces[p].visible = 0;
    }
    //console.log("ar trebui sa fie doar una vizibila 100%");
}

function checkCollision(obj, x, y) {
    if (!obj.solved) {
        if (y > obj.y + obj.h) {
            return (0);
        }
        //y top edge
        if (y < obj.y) {
            return (0);
        }
        //x right edge
        if (x > obj.x + obj.w) {
            return (0);
        }
        //x left edge
        if (x < obj.x) {
            return (0);
        }
        return (1); //collision
    } else {
        return (0);
    }
}

function clickDown(e) {
    //console.log("click1");
    e.preventDefault();
    var rect = puzzleObj.canvas.getBoundingClientRect();
    var x = e.clientX - rect.left;
    var y = e.clientY - rect.top;
    if (typeof e.changedTouches !== 'undefined') {
        x = e.changedTouches[0].pageX - rect.left;
        y = e.changedTouches[0].pageY - rect.top;
    }
    //console.log(x,y);
    return ([x, y]);
}

function clickPiece(x, y) {
    //console.log("click");
    for (var i = puzzleObj.pieces.length - 1; i >= 0; i--) {
        if (checkCollision(puzzleObj.pieces[i], x, y)) {
            puzzleObj.clickedpiece = i;
            hideAllPieces();
            puzzleObj.pieces[i].visible = 1;
            puzzleObj.pieces[i].offsetx = x - puzzleObj.pieces[i].x;
            puzzleObj.pieces[i].offsety = y - puzzleObj.pieces[i].y;
            break;
        }
    }
    drawPieces();
}


function releasePiece() {
    if (puzzleObj.clickedpiece !== -1) {
        //move selected piece to the end of the array - makes last touched piece always be on top
        var tmp = puzzleObj.pieces[puzzleObj.clickedpiece];
        puzzleObj.pieces.splice(puzzleObj.clickedpiece, 1);
        puzzleObj.pieces.push(tmp);


        for (var p = 0; p < puzzleObj.pieces.length; p++) {
            puzzleObj.pieces[p].visible = 1;
        }
        puzzleObj.pieces[puzzleObj.clickedpiece].offsetx = 0;
        puzzleObj.pieces[puzzleObj.clickedpiece].offsety = 0;
        checkSolved();
        puzzleObj.clickedpiece = -1;

        if (puzzleObj.pieces.length === 0) {
            document.getElementById("message").innerHTML = "Good job!";

            setTimeout(function() {
                document.getElementById("message").innerHTML = '';
            }, 2000);
            drawPieces();
        }

    }
    drawPieces();
}

function movePiece(e) {
    e.preventDefault();
    var movement = clickDown(e);
    puzzleObj.pieces[puzzleObj.clickedpiece].x = movement[0] - puzzleObj.pieces[puzzleObj.clickedpiece].offsetx;
    puzzleObj.pieces[puzzleObj.clickedpiece].y = movement[1] - puzzleObj.pieces[puzzleObj.clickedpiece].offsety;
    drawPieces();
}


function checkSolved() {
    //console.log(puzzleObj.pieces[puzzleObj.clickedpiece].x,puzzleObj.pieces[puzzleObj.clickedpiece].solvedx);

    var newx = puzzleObj.pieces[puzzleObj.clickedpiece].x;
    var newy = puzzleObj.pieces[puzzleObj.clickedpiece].y;
    var sx = puzzleObj.pieces[puzzleObj.clickedpiece].solvedx;
    var sy = puzzleObj.pieces[puzzleObj.clickedpiece].solvedy;

    var tolerance = 50;
    //console.log(newx,sx);

    //if the piece is solved
    if (Math.abs(newx - sx) < tolerance && Math.abs(newy - sy) < tolerance) {
        puzzleObj.pieces[puzzleObj.clickedpiece].x = sx;
        puzzleObj.pieces[puzzleObj.clickedpiece].y = sy;
        puzzleObj.pieces[puzzleObj.clickedpiece].solved = 1;

        var tmp = puzzleObj.pieces[puzzleObj.clickedpiece];

        puzzleObj.pieces.splice(puzzleObj.clickedpiece, 1);
        puzzleObj.solvedpieces.push(tmp);
        var txt1=document.getElementById("message1").innerHTML= puzzleObj.solvedpieces.length + " from " + puzzleObj.numberPiecesX * puzzleObj.numberPiecesY;
    }
}
// ----- upload imagine -----

function upload(e) {
    let t = e.target || window.event.srcElement,
        files = t.files;

    if (FileReader && files && files.length) {
        var fr = new FileReader();

        fr.onload = function() {
            puzzles.push({
                id: index,
                name: fr.result, 
                move: puzzleObj,
                status: 0
            });;
            index++;
            localStorage.setItem('puzzles', JSON.stringify(puzzles));
            const bla = localStorage.getItem('puzzles');
        }
        fr.readAsDataURL(files[0]);

    }
    document.getElementById("choose1").innerHTML = "Image selected";
    document.getElementById("startGame").disabled = false;
}

function readURL(event) {
    var selectedFile = event.target;
    var image = document.getElementById('urlLink').value;
    image.name = selectedFile;

    puzzles.push({
        id: index,
        name: image,
        move: puzzleObj,
        status: 0
    });;
    index++;
    console.log(image);
    localStorage.setItem('puzzles', JSON.stringify(puzzles));
    const bla = localStorage.getItem('puzzles');
    document.getElementById("startGame").disabled = false;
}
var resize;
window.addEventListener('resize', function(event) {
    clearTimeout(resize);
    resize = setTimeout(resizeGame(), 200);
});

function resizeGame() {
    initCanvasSize();
    var diffx = (puzzleObj.canvasw / puzzleObj.savedcanvasw) * 100;
    var diffy = (puzzleObj.canvash / puzzleObj.savedcanvash) * 100;
    for (var p = 0; p < puzzleObj.pieces.length; p++) {
        puzzleObj.pieces[p].x = (puzzleObj.pieces[p].x / 100) * diffx;
        puzzleObj.pieces[p].y = (puzzleObj.pieces[p].y / 100) * diffy;
        puzzleObj.pieces[p].w = (puzzleObj.pieces[p].w / 100) * diffx;
        puzzleObj.pieces[p].h = (puzzleObj.pieces[p].h / 100) * diffy;
        puzzleObj.pieces[p].solvedx = (puzzleObj.pieces[p].solvedx / 100) * diffx;
        puzzleObj.pieces[p].solvedy = (puzzleObj.pieces[p].solvedy / 100) * diffy;
    }
    for (p = 0; p < puzzleObj.solvedpieces.length; p++) {
        puzzleObj.solvedpieces[p].x = (puzzleObj.solvedpieces[p].x / 100) * diffx;
        puzzleObj.solvedpieces[p].y = (puzzleObj.solvedpieces[p].y / 100) * diffy;
        puzzleObj.solvedpieces[p].w = (puzzleObj.solvedpieces[p].w / 100) * diffx;
        puzzleObj.solvedpieces[p].h = (puzzleObj.solvedpieces[p].h / 100) * diffy;
        puzzleObj.solvedpieces[p].solvedx = (puzzleObj.solvedpieces[p].solvedx / 100) * diffx;
        puzzleObj.solvedpieces[p].solvedy = (puzzleObj.solvedpieces[p].solvedy / 100) * diffy;
    }
    puzzleObj.savedcanvasw = puzzleObj.canvasw;
    puzzleObj.savedcanvash = puzzleObj.canvash;
    //events();
    drawPieces();
}

